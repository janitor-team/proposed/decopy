## Things that still need to be done

 * files wildcarding: `foo.*`, for `foo.c` and `foo.h`, take into account
   `foo.m` (that might be under a different license and listed first).

 * diff mode: generate the output as a temporary file and then diff it against
   debian/copyright.

 * explain mode: list the differences with the current information in the
   debian/copyright

 * Read the AUTHORS file and use it in a similar way as the COPYING file.

 * The license field in a Files paragraph is not being properly parsed ('and's
   and commas are not taken into account). The rules are defined in the section
   "7.2 Syntax" of the copyright format.
