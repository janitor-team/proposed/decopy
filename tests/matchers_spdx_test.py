#!/usr/bin/env python3
# encoding: utf-8

# Unit tests for decopy
# Copyright (C) 2018 Maximiliano Curia <maxy@debian.org>
#
# License: ISC
#  Permission to use, copy, modify, and/or distribute this software for any
#  purpose with or without fee is hereby granted, provided that the above
#  copyright notice and this permission notice appear in all copies.
#  .
#  THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
#  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
#  AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT,
#  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
#  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
#  OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
#  PERFORMANCE OF THIS SOFTWARE.

import json
import os
import unittest

from decopy import matchers


class TestSpdx(unittest.TestCase):

    def _check_license_match(self, license_, matcher, expected, ignore=None):

        for key in ('licenseText', 'standardLicenseHeader',
                    'licenseExceptionText'):
            if key not in license_ or not license_[key]:
                continue
            if ignore and key in ignore:
                continue
            text = license_[key]
            result = matcher(text)
            self.assertEqual(result, expected,
                             msg='with key={}'.format(key))

    def _open_license_and_check(self, filename, matcher, expected, ignore=None):

        with open(os.path.join('tests/testdata/licenses', filename)) as fp:
            license_ = json.load(fp)
        return self._check_license_match(license_, matcher, expected, ignore)

    @staticmethod
    def _find_licenses_matcher(text):
        licenses = matchers.find_licenses(
            matchers.clean_comments(text))
        return ' or '.join(sorted(licenses))

    def testSpdxA(self):

        self._open_license_and_check('AAL.json',
                                     self._find_licenses_matcher,
                                     'AAL')
        self._open_license_and_check('Abstyles.json',
                                     self._find_licenses_matcher,
                                     'Abstyles')
        self._open_license_and_check('Adobe-2006.json',
                                     self._find_licenses_matcher,
                                     'Adobe-2006')
        self._open_license_and_check('Adobe-Glyph.json',
                                     self._find_licenses_matcher,
                                     'Adobe-Glyph')
        self._open_license_and_check('ADSL.json',
                                     self._find_licenses_matcher,
                                     'ADSL')
        self._open_license_and_check('AFL-1.1.json',
                                     self._find_licenses_matcher,
                                     'Academic-1.1')
        self._open_license_and_check('AFL-1.2.json',
                                     self._find_licenses_matcher,
                                     'Academic-1.2')
        self._open_license_and_check('AFL-2.0.json',
                                     self._find_licenses_matcher,
                                     'Academic-2')
        self._open_license_and_check('AFL-2.1.json',
                                     self._find_licenses_matcher,
                                     'Academic-2.1')
        self._open_license_and_check('AFL-3.0.json',
                                     self._find_licenses_matcher,
                                     'Academic-3')
        self._open_license_and_check('Afmparse.json',
                                     self._find_licenses_matcher,
                                     'Afmparse')
        self._open_license_and_check('Aladdin.json',
                                     self._find_licenses_matcher,
                                     'Aladdin')
        self._open_license_and_check('AMDPLPA.json',
                                     self._find_licenses_matcher,
                                     'AMDPLPA')
        self._open_license_and_check('AML.json',
                                     self._find_licenses_matcher,
                                     'AML')
        self._open_license_and_check('AMPAS.json',
                                     self._find_licenses_matcher,
                                     'BSD-2-clause-AMPAS')
        self._open_license_and_check('ANTLR-PD.json',
                                     self._find_licenses_matcher,
                                     'ANTLR-PD')
        self._open_license_and_check('Apache-1.0.json',
                                     self._find_licenses_matcher,
                                     'Apache-1')
        self._open_license_and_check('Apache-1.1.json',
                                     self._find_licenses_matcher,
                                     'Apache-1.1')
        self._open_license_and_check('Apache-2.0.json',
                                     self._find_licenses_matcher,
                                     'Apache-2')
        self._open_license_and_check('APAFML.json',
                                     self._find_licenses_matcher,
                                     'APAFML')
        self._open_license_and_check('APL-1.0.json',
                                     self._find_licenses_matcher,
                                     'APL-1',
                                     ignore={'standardLicenseHeader'})
        self._open_license_and_check('APSL-1.0.json',
                                     self._find_licenses_matcher,
                                     'APSL-1')
        self._open_license_and_check('APSL-1.1.json',
                                     self._find_licenses_matcher,
                                     'APSL-1.1')
        self._open_license_and_check('APSL-1.2.json',
                                     self._find_licenses_matcher,
                                     'APSL-1.2')
        self._open_license_and_check('APSL-2.0.json',
                                     self._find_licenses_matcher,
                                     'APSL-2')
        self._open_license_and_check('Artistic-1.0-cl8.json',
                                     self._find_licenses_matcher,
                                     'Artistic-1-cl8')
        self._open_license_and_check('Artistic-1.0-Perl.json',
                                     self._find_licenses_matcher,
                                     'Artistic-1-Perl')
        self._open_license_and_check('Artistic-1.0.json',
                                     self._find_licenses_matcher,
                                     'Artistic-1')
        self._open_license_and_check('Artistic-2.0.json',
                                     self._find_licenses_matcher,
                                     'Artistic-2')

    def testSpdxB(self):
        self._open_license_and_check('Bahyph.json',
                                     self._find_licenses_matcher,
                                     'Bahyph')
        self._open_license_and_check('Barr.json',
                                     self._find_licenses_matcher,
                                     'Barr')
        self._open_license_and_check('BitTorrent-1.0.json',
                                     self._find_licenses_matcher,
                                     'BitTorrent-1')
        # The header has the wrong version in it
        self._open_license_and_check('BitTorrent-1.1.json',
                                     self._find_licenses_matcher,
                                     'BitTorrent-1')
        self._open_license_and_check('Borceux.json',
                                     self._find_licenses_matcher,
                                     'Borceux')
        self._open_license_and_check('BSL-1.0.json',
                                     self._find_licenses_matcher,
                                     'BSL-1')
        self._open_license_and_check('bzip2-1.0.5.json',
                                     self._find_licenses_matcher,
                                     'BSD-like-bzip2')
        self._open_license_and_check('bzip2-1.0.6.json',
                                     self._find_licenses_matcher,
                                     'BSD-like-bzip2')

    def testSpdxC(self):
        self._open_license_and_check('Caldera.json',
                                     self._find_licenses_matcher,
                                     'Caldera')
        self._open_license_and_check('CATOSL-1.1.json',
                                     self._find_licenses_matcher,
                                     'CATOSL-1.1')
        self._open_license_and_check('CDDL-1.0.json',
                                     self._find_licenses_matcher,
                                     'CDDL-1')
        self._open_license_and_check('CDDL-1.1.json',
                                     self._find_licenses_matcher,
                                     'CDDL-1.1')
        self._open_license_and_check('CECILL-1.0.json',
                                     self._find_licenses_matcher,
                                     'CeCILL-1')
        self._open_license_and_check('CECILL-1.1.json',
                                     self._find_licenses_matcher,
                                     'CeCILL-1.1')
        self._open_license_and_check('CECILL-2.0.json',
                                     self._find_licenses_matcher,
                                     'CeCILL-2')
        self._open_license_and_check('CECILL-2.1.json',
                                     self._find_licenses_matcher,
                                     'CeCILL-2.1')
        self._open_license_and_check('CECILL-B.json',
                                     self._find_licenses_matcher,
                                     'CeCILL-B')
        self._open_license_and_check('CECILL-C.json',
                                     self._find_licenses_matcher,
                                     'CeCILL-C')

        self._open_license_and_check('ClArtistic.json',
                                     self._find_licenses_matcher,
                                     'ClArtistic')

        self._open_license_and_check('CNRI-Jython.json',
                                     self._find_licenses_matcher,
                                     'CNRI-Jython')
        self._open_license_and_check('CNRI-Python-GPL-Compatible.json',
                                     self._find_licenses_matcher,
                                     'CNRI-Python-GPL-Compatible')
        self._open_license_and_check('CNRI-Python.json',
                                     self._find_licenses_matcher,
                                     'CNRI-Python')

        self._open_license_and_check('Condor-1.1.json',
                                     self._find_licenses_matcher,
                                     'Condor-1.1+')

        self._open_license_and_check('CPAL-1.0.json',
                                     self._find_licenses_matcher,
                                     'CPAL-1')
        self._open_license_and_check('CPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'CPL-1')
        self._open_license_and_check('CPOL-1.02.json',
                                     self._find_licenses_matcher,
                                     'CPOL-1.02')
        self._open_license_and_check('Crossword.json',
                                     self._find_licenses_matcher,
                                     'Crossword')
        self._open_license_and_check('CrystalStacker.json',
                                     self._find_licenses_matcher,
                                     'CrystalStacker')
        self._open_license_and_check('CUA-OPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'CUA-OPL-1')
        self._open_license_and_check('Cube.json',
                                     self._find_licenses_matcher,
                                     'Cube')
        self._open_license_and_check('curl.json',
                                     self._find_licenses_matcher,
                                     'curl')

    def testSpdxD(self):
        self._open_license_and_check('D-FSL-1.0.json',
                                     self._find_licenses_matcher,
                                     'D-FSL')
        self._open_license_and_check('diffmark.json',
                                     self._find_licenses_matcher,
                                     'diffmark')
        self._open_license_and_check('DOC.json',
                                     self._find_licenses_matcher,
                                     'DOC')
        self._open_license_and_check('Dotseqn.json',
                                     self._find_licenses_matcher,
                                     'Dotseqn')
        self._open_license_and_check('DSDP.json',
                                     self._find_licenses_matcher,
                                     'DSDP')
        self._open_license_and_check('dvipdfm.json',
                                     self._find_licenses_matcher,
                                     'dvipdfm')

    def testSpdxE(self):
        self._open_license_and_check('ECL-1.0.json',
                                     self._find_licenses_matcher,
                                     'ECL-1')
        self._open_license_and_check('ECL-2.0.json',
                                     self._find_licenses_matcher,
                                     'ECL-2')
        self._open_license_and_check('eCos-2.0.json',
                                     self._find_licenses_matcher,
                                     'GPL-2+ with eCos-2 exception')
        self._open_license_and_check('EFL-1.0.json',
                                     self._find_licenses_matcher,
                                     'EFL-1')
        self._open_license_and_check('EFL-2.0.json',
                                     self._find_licenses_matcher,
                                     'EFL-2')
        self._open_license_and_check('eGenix.json',
                                     self._find_licenses_matcher,
                                     'eGenix')
        self._open_license_and_check('Entessa.json',
                                     self._find_licenses_matcher,
                                     'BSD-like-Entessa')
        self._open_license_and_check('EPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'EPL-1')
        self._open_license_and_check('ErlPL-1.1.json',
                                     self._find_licenses_matcher,
                                     'ErlPL-1.1')
        self._open_license_and_check('EUDatagrid.json',
                                     self._find_licenses_matcher,
                                     'EUDatagrid')
        self._open_license_and_check('EUPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'EUPL-1')
        self._open_license_and_check('EUPL-1.1.json',
                                     self._find_licenses_matcher,
                                     'EUPL-1.1')
        self._open_license_and_check('Eurosym.json',
                                     self._find_licenses_matcher,
                                     'Eurosym')

    def testSpdxF(self):
        self._open_license_and_check('Fair.json',
                                     self._find_licenses_matcher,
                                     'Fair')
        self._open_license_and_check('Frameworx-1.0.json',
                                     self._find_licenses_matcher,
                                     'Frameworx-1')
        self._open_license_and_check('FreeImage.json',
                                     self._find_licenses_matcher,
                                     'FreeImage-1')
        self._open_license_and_check('FSFAP.json',
                                     self._find_licenses_matcher,
                                     'FSFAP')
        self._open_license_and_check('FSFUL.json',
                                     self._find_licenses_matcher,
                                     'FSFUL')
        self._open_license_and_check('FSFULLR.json',
                                     self._find_licenses_matcher,
                                     'FSFULLR')
        self._open_license_and_check('FTL.json',
                                     self._find_licenses_matcher,
                                     'FTL')

    def testSpdxG(self):
        self._open_license_and_check('GFDL-1.1.json',
                                     self._find_licenses_matcher,
                                     'GFDL-1.1')
        self._open_license_and_check('GFDL-1.2.json',
                                     self._find_licenses_matcher,
                                     'GFDL-NIV-1.2')
        self._open_license_and_check('GFDL-1.3.json',
                                     self._find_licenses_matcher,
                                     'GFDL-NIV-1.3')
        self._open_license_and_check('Giftware.json',
                                     self._find_licenses_matcher,
                                     'Giftware')
        self._open_license_and_check('GL2PS.json',
                                     self._find_licenses_matcher,
                                     'GL2PS')
        self._open_license_and_check('Glide.json',
                                     self._find_licenses_matcher,
                                     'Glide')
        self._open_license_and_check('Glulxe.json',
                                     self._find_licenses_matcher,
                                     'Glulxe')
        self._open_license_and_check('gnuplot.json',
                                     self._find_licenses_matcher,
                                     'gnuplot')
        self._open_license_and_check('gSOAP-1.3b.json',
                                     self._find_licenses_matcher,
                                     'gSOAP-1.3')

    def testSpdxH(self):
        self._open_license_and_check('HaskellReport.json',
                                     self._find_licenses_matcher,
                                     'HaskellReport')
        self._open_license_and_check('HPND.json',
                                     self._find_licenses_matcher,
                                     'HPND')

    def testSpdxI(self):
        self._open_license_and_check('IBM-pibs.json',
                                     self._find_licenses_matcher,
                                     'IBM-pibs')
        self._open_license_and_check('ICU.json',
                                     self._find_licenses_matcher,
                                     'MIT-like-icu')
        self._open_license_and_check('IJG.json',
                                     self._find_licenses_matcher,
                                     'IJG')
        self._open_license_and_check('ImageMagick.json',
                                     self._find_licenses_matcher,
                                     'ImageMagick')
        self._open_license_and_check('iMatix.json',
                                     self._find_licenses_matcher,
                                     'iMatix')
        self._open_license_and_check('Imlib2.json',
                                     self._find_licenses_matcher,
                                     'MIT-Imlib2')
        self._open_license_and_check('Info-ZIP.json',
                                     self._find_licenses_matcher,
                                     'Info-ZIP')
        self._open_license_and_check('Intel-ACPI.json',
                                     self._find_licenses_matcher,
                                     'Intel-ACPI')
        self._open_license_and_check('Intel.json',
                                     self._find_licenses_matcher,
                                     'Intel')
        self._open_license_and_check('Interbase-1.0.json',
                                     self._find_licenses_matcher,
                                     'Interbase-1')
        self._open_license_and_check('IPA.json',
                                     self._find_licenses_matcher,
                                     'IPA-1')
        self._open_license_and_check('IPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'IPL-1')
        self._open_license_and_check('ISC.json',
                                     self._find_licenses_matcher,
                                     'ISC')

    def testSpdxJ(self):
        self._open_license_and_check('JasPer-2.0.json',
                                     self._find_licenses_matcher,
                                     'JasPer-2')
        self._open_license_and_check('JSON.json',
                                     self._find_licenses_matcher,
                                     'JSON')

    def testSpdxL(self):
        self._open_license_and_check('LAL-1.2.json',
                                     self._find_licenses_matcher,
                                     'LAL-1.2')
        self._open_license_and_check('LAL-1.3.json',
                                     self._find_licenses_matcher,
                                     'LAL-1.3')
        self._open_license_and_check('Latex2e.json',
                                     self._find_licenses_matcher,
                                     'Latex2e')
        self._open_license_and_check('Leptonica.json',
                                     self._find_licenses_matcher,
                                     'Leptonica')
        self._open_license_and_check('Libpng.json',
                                     self._find_licenses_matcher,
                                     'Libpng')
        self._open_license_and_check('libtiff.json',
                                     self._find_licenses_matcher,
                                     'libtiff')
        self._open_license_and_check('LiLiQ-P-1.1.json',
                                     self._find_licenses_matcher,
                                     'LiLiQ-P-1.1')
        self._open_license_and_check('LiLiQ-R-1.1.json',
                                     self._find_licenses_matcher,
                                     'LiLiQ-R-1.1')
        self._open_license_and_check('LiLiQ-Rplus-1.1.json',
                                     self._find_licenses_matcher,
                                     'LiLiQ-R+-1.1')
        self._open_license_and_check('LPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'LPL-1')
        self._open_license_and_check('LPL-1.02.json',
                                     self._find_licenses_matcher,
                                     'LPL-1.02')

    def testSpdxM(self):
        self._open_license_and_check('MakeIndex.json',
                                     self._find_licenses_matcher,
                                     'MakeIndex')
        self._open_license_and_check('MirOS.json',
                                     self._find_licenses_matcher,
                                     'MirOS')
        self._open_license_and_check('Motosoto.json',
                                     self._find_licenses_matcher,
                                     'Motosoto',
                                     ignore={'standardLicenseHeader'})
        self._open_license_and_check('mpich2.json',
                                     self._find_licenses_matcher,
                                     'mpich')
        self._open_license_and_check('MPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'MPL-1')
        self._open_license_and_check('MPL-1.1.json',
                                     self._find_licenses_matcher,
                                     'MPL-1.1')
        self._open_license_and_check('MPL-2.0-no-copyleft-exception.json',
                                     self._find_licenses_matcher,
                                     'MPL-2 with no-copyleft exception',
                                     ignore={'licenseText'})
        self._open_license_and_check('MPL-2.0.json',
                                     self._find_licenses_matcher,
                                     'MPL-2')
        self._open_license_and_check('MS-PL.json',
                                     self._find_licenses_matcher,
                                     'MS-PL')
        self._open_license_and_check('MS-RL.json',
                                     self._find_licenses_matcher,
                                     'MS-RL')
        self._open_license_and_check('MTLL.json',
                                     self._find_licenses_matcher,
                                     'MTLL')
        self._open_license_and_check('Multics.json',
                                     self._find_licenses_matcher,
                                     'Multics')
        self._open_license_and_check('Mup.json',
                                     self._find_licenses_matcher,
                                     'BSD-like-Mup')

    def testSpdxN(self):
        self._open_license_and_check('NASA-1.3.json',
                                     self._find_licenses_matcher,
                                     'NASA-1.3')
        self._open_license_and_check('Naumen.json',
                                     self._find_licenses_matcher,
                                     'Naumen')
        self._open_license_and_check('NBPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'NBPL-1')
        self._open_license_and_check('NCSA.json',
                                     self._find_licenses_matcher,
                                     'NCSA')
        self._open_license_and_check('NetCDF.json',
                                     self._find_licenses_matcher,
                                     'NetCDF')
        self._open_license_and_check('Newsletr.json',
                                     self._find_licenses_matcher,
                                     'Newsletr')
        self._open_license_and_check('NGPL.json',
                                     self._find_licenses_matcher,
                                     'NGPL')
        self._open_license_and_check('NLOD-1.0.json',
                                     self._find_licenses_matcher,
                                     'NLOD-1')
        self._open_license_and_check('NLPL.json',
                                     self._find_licenses_matcher,
                                     'NLPL')
        self._open_license_and_check('Nokia.json',
                                     self._find_licenses_matcher,
                                     'NOKOS-1')
        self._open_license_and_check('NOSL.json',
                                     self._find_licenses_matcher,
                                     'NOSL-1')
        self._open_license_and_check('Noweb.json',
                                     self._find_licenses_matcher,
                                     'Noweb')
        self._open_license_and_check('NPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'NPL-1')
        # The header included in the file is NPL-1
        self._open_license_and_check('NPL-1.1.json',
                                     self._find_licenses_matcher,
                                     'MPL-1.1_NPL-1.1',
                                     ignore={'standardLicenseHeader'})
        self._open_license_and_check('NPOSL-3.0.json',
                                     self._find_licenses_matcher,
                                     'NPOSL-3')
        self._open_license_and_check('NRL.json',
                                     self._find_licenses_matcher,
                                     'NRL')
        self._open_license_and_check('NTP.json',
                                     self._find_licenses_matcher,
                                     'NTP')
        self._open_license_and_check('Nunit.json',
                                     self._find_licenses_matcher,
                                     'Zlib-acknowledgement')

    def testSpdxO(self):
        self._open_license_and_check('OCCT-PL.json',
                                     self._find_licenses_matcher,
                                     'OCCT-PL')
        self._open_license_and_check('OCLC-2.0.json',
                                     self._find_licenses_matcher,
                                     'OCLC-2')
        self._open_license_and_check('ODbL-1.0.json',
                                     self._find_licenses_matcher,
                                     'ODbL-1')
        self._open_license_and_check('OFL-1.0.json',
                                     self._find_licenses_matcher,
                                     'OFL-1')
        self._open_license_and_check('OFL-1.1.json',
                                     self._find_licenses_matcher,
                                     'OFL-1.1')
        self._open_license_and_check('OGTSL.json',
                                     self._find_licenses_matcher,
                                     'OGTSL')
        self._open_license_and_check('OLDAP-1.1.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-1.1')
        self._open_license_and_check('OLDAP-1.2.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-1.2')
        self._open_license_and_check('OLDAP-1.3.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-1.3')
        self._open_license_and_check('OLDAP-1.4.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-1.4')
        self._open_license_and_check('OLDAP-2.0.1.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.0.1')
        self._open_license_and_check('OLDAP-2.0.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2')
        self._open_license_and_check('OLDAP-2.1.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.1')
        self._open_license_and_check('OLDAP-2.2.1.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.2.1')
        self._open_license_and_check('OLDAP-2.2.2.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.2.2')
        self._open_license_and_check('OLDAP-2.2.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.2')
        self._open_license_and_check('OLDAP-2.3.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.3')
        self._open_license_and_check('OLDAP-2.4.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.4')
        self._open_license_and_check('OLDAP-2.5.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.5')
        self._open_license_and_check('OLDAP-2.6.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.6')
        self._open_license_and_check('OLDAP-2.7.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.7')
        self._open_license_and_check('OLDAP-2.8.json',
                                     self._find_licenses_matcher,
                                     'OLDAP-2.8')
        self._open_license_and_check('OML.json',
                                     self._find_licenses_matcher,
                                     'OML')
        self._open_license_and_check('OpenSSL.json',
                                     self._find_licenses_matcher,
                                     'OpenSSL or SSLeay')
        self._open_license_and_check('OPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'OPL-1')
        self._open_license_and_check('OSET-PL-2.1.json',
                                     self._find_licenses_matcher,
                                     'OSET-PL-2.1')
        self._open_license_and_check('OSL-1.0.json',
                                     self._find_licenses_matcher,
                                     'OSL-1')
        self._open_license_and_check('OSL-1.1.json',
                                     self._find_licenses_matcher,
                                     'OSL-1.1')
        self._open_license_and_check('OSL-2.0.json',
                                     self._find_licenses_matcher,
                                     'OSL-2')
        self._open_license_and_check('OSL-2.1.json',
                                     self._find_licenses_matcher,
                                     'OSL-2.1')
        self._open_license_and_check('OSL-3.0.json',
                                     self._find_licenses_matcher,
                                     'OSL-3')

    def testSpdxP(self):
        self._open_license_and_check('PDDL-1.0.json',
                                     self._find_licenses_matcher,
                                     'PDDL-1')
        self._open_license_and_check('PHP-3.0.json',
                                     self._find_licenses_matcher,
                                     'PHP-3')
        self._open_license_and_check('PHP-3.01.json',
                                     self._find_licenses_matcher,
                                     'PHP-3.01')
        self._open_license_and_check('Plexus.json',
                                     self._find_licenses_matcher,
                                     'Plexus')
        self._open_license_and_check('PostgreSQL.json',
                                     self._find_licenses_matcher,
                                     'PostgreSQL')
        self._open_license_and_check('psfrag.json',
                                     self._find_licenses_matcher,
                                     'psfrag')
        self._open_license_and_check('psutils.json',
                                     self._find_licenses_matcher,
                                     'psutils')
        # The MIT/X11 part could do with a more specific name
        self._open_license_and_check('Python-2.0.json',
                                     self._find_licenses_matcher,
                                     'CNRI-Python or MIT/X11 or Python-2')

    def testSpdxQ(self):
        self._open_license_and_check('Qhull.json',
                                     self._find_licenses_matcher,
                                     'Qhull')
        self._open_license_and_check('QPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'QPL-1')

    def testSpdxR(self):
        self._open_license_and_check('Rdisc.json',
                                     self._find_licenses_matcher,
                                     'Rdisc')
        self._open_license_and_check('RHeCos-1.1.json',
                                     self._find_licenses_matcher,
                                     'RHeCos-1.1')
        self._open_license_and_check('RPL-1.1.json',
                                     self._find_licenses_matcher,
                                     'RPL-1.1')
        self._open_license_and_check('RPL-1.5.json',
                                     self._find_licenses_matcher,
                                     'RPL-1.5')
        self._open_license_and_check('RPSL-1.0.json',
                                     self._find_licenses_matcher,
                                     'RPSL-1')
        self._open_license_and_check('RSA-MD.json',
                                     self._find_licenses_matcher,
                                     'RSA-MD')
        self._open_license_and_check('RSCPL.json',
                                     self._find_licenses_matcher,
                                     'RSCPL-1')
        self._open_license_and_check('Ruby.json',
                                     self._find_licenses_matcher,
                                     'Ruby')

    def testSpdxS(self):
        self._open_license_and_check('SAX-PD.json',
                                     self._find_licenses_matcher,
                                     'public-domain')
        self._open_license_and_check('Saxpath.json',
                                     self._find_licenses_matcher,
                                     'BSD-like-Saxpath')
        self._open_license_and_check('SCEA.json',
                                     self._find_licenses_matcher,
                                     'SCEA-1')
        self._open_license_and_check('Sendmail.json',
                                     self._find_licenses_matcher,
                                     'Sendmail')
        self._open_license_and_check('SGI-B-1.0.json',
                                     self._find_licenses_matcher,
                                     'SGI-B-1')
        self._open_license_and_check('SGI-B-1.1.json',
                                     self._find_licenses_matcher,
                                     'SGI-B-1.1')
        self._open_license_and_check('SGI-B-2.0.json',
                                     self._find_licenses_matcher,
                                     'SGI-B-2')
        self._open_license_and_check('SimPL-2.0.json',
                                     self._find_licenses_matcher,
                                     'SimPL-2')
        self._open_license_and_check('SISSL-1.2.json',
                                     self._find_licenses_matcher,
                                     'SISSL-1.2')
        self._open_license_and_check('SISSL.json',
                                     self._find_licenses_matcher,
                                     'SISSL-1.1')
        self._open_license_and_check('Sleepycat.json',
                                     self._find_licenses_matcher,
                                     'Sleepycat')
        self._open_license_and_check('SMLNJ.json',
                                     self._find_licenses_matcher,
                                     'MIT-like-SMLNJ')
        self._open_license_and_check('SMPPL.json',
                                     self._find_licenses_matcher,
                                     'SMPPL')
        self._open_license_and_check('SNIA.json',
                                     self._find_licenses_matcher,
                                     'SNIA-1')
        self._open_license_and_check('Spencer-86.json',
                                     self._find_licenses_matcher,
                                     'Spencer-86')
        self._open_license_and_check('Spencer-94.json',
                                     self._find_licenses_matcher,
                                     'Spencer-94')
        self._open_license_and_check('Spencer-99.json',
                                     self._find_licenses_matcher,
                                     'Spencer-99')
        self._open_license_and_check('SPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'SPL-1')
        self._open_license_and_check('StandardML-NJ.json',
                                     self._find_licenses_matcher,
                                     'MIT-like-SMLNJ')
        self._open_license_and_check('SugarCRM-1.1.3.json',
                                     self._find_licenses_matcher,
                                     'SugarCRM-1.1.3')
        self._open_license_and_check('SWL.json',
                                     self._find_licenses_matcher,
                                     'SWL')

    def testSpdxT(self):
        self._open_license_and_check('TCL.json',
                                     self._find_licenses_matcher,
                                     'TCL')
        self._open_license_and_check('TMate.json',
                                     self._find_licenses_matcher,
                                     'BSD-like-TMate')
        self._open_license_and_check('TORQUE-1.1.json',
                                     self._find_licenses_matcher,
                                     'TORQUE-1.1')
        self._open_license_and_check('TOSL.json',
                                     self._find_licenses_matcher,
                                     'BSD-like-TOSL')

    def testSpdxU(self):
        self._open_license_and_check('Unicode-TOU.json',
                                     self._find_licenses_matcher,
                                     'Unicode-TOU')
        self._open_license_and_check('Unlicense.json',
                                     self._find_licenses_matcher,
                                     'Unlicense')
        self._open_license_and_check('UPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'UPL-1')

    def testSpdxV(self):
        self._open_license_and_check('Vim.json',
                                     self._find_licenses_matcher,
                                     'Vim')
        self._open_license_and_check('VOSTROM.json',
                                     self._find_licenses_matcher,
                                     'VOSTROM')
        self._open_license_and_check('VSL-1.0.json',
                                     self._find_licenses_matcher,
                                     'VSL-1')

    def testSpdxW(self):
        # self._open_license_and_check('W3C-19980720.json',
        #                              self._find_licenses_matcher,
        #                              'W3C-19980720')
        self._open_license_and_check('W3C.json',
                                     self._find_licenses_matcher,
                                     'W3C',
                                     ignore={"standardLicenseHeader"})
        self._open_license_and_check('Watcom-1.0.json',
                                     self._find_licenses_matcher,
                                     'Watcom-1')
        self._open_license_and_check('Wsuipa.json',
                                     self._find_licenses_matcher,
                                     'Wsuipa')
        self._open_license_and_check('WTFPL.json',
                                     self._find_licenses_matcher,
                                     'WTFPL')
        self._open_license_and_check('WXwindows.json',
                                     self._find_licenses_matcher,
                                     'LGPL-2+ with WxWindows-3.1+ exception')

    def testSpdxX(self):
        self._open_license_and_check('X11.json',
                                     self._find_licenses_matcher,
                                     'X11')
        self._open_license_and_check('Xerox.json',
                                     self._find_licenses_matcher,
                                     'Xerox')
        self._open_license_and_check('XFree86-1.1.json',
                                     self._find_licenses_matcher,
                                     'XFree86-1.1')
        self._open_license_and_check('xinetd.json',
                                     self._find_licenses_matcher,
                                     'xinetd')
        self._open_license_and_check('Xnet.json',
                                     self._find_licenses_matcher,
                                     'Expat')
        self._open_license_and_check('xpp.json',
                                     self._find_licenses_matcher,
                                     'xpp')
        self._open_license_and_check('XSkat.json',
                                     self._find_licenses_matcher,
                                     'XSkat')

    def testSpdxY(self):
        self._open_license_and_check('YPL-1.0.json',
                                     self._find_licenses_matcher,
                                     'YPL-1')
        self._open_license_and_check('YPL-1.1.json',
                                     self._find_licenses_matcher,
                                     'YPL-1.1')

    def testSpdxZ(self):
        self._open_license_and_check('Zed.json',
                                     self._find_licenses_matcher,
                                     'Zed')
        self._open_license_and_check('Zend-2.0.json',
                                     self._find_licenses_matcher,
                                     'Zend-2')
        self._open_license_and_check('Zimbra-1.3.json',
                                     self._find_licenses_matcher,
                                     'Zimbra-1.3')
        self._open_license_and_check('Zimbra-1.4.json',
                                     self._find_licenses_matcher,
                                     'Zimbra-1.4')
        self._open_license_and_check('zlib-acknowledgement.json',
                                     self._find_licenses_matcher,
                                     'Zlib-acknowledgement')
        self._open_license_and_check('Zlib.json',
                                     self._find_licenses_matcher,
                                     'Zlib')
        self._open_license_and_check('ZPL-1.1.json',
                                     self._find_licenses_matcher,
                                     'ZPL-1.1')
        self._open_license_and_check('ZPL-2.0.json',
                                     self._find_licenses_matcher,
                                     'ZPL-2')
        self._open_license_and_check('ZPL-2.1.json',
                                     self._find_licenses_matcher,
                                     'ZPL-2.1')


if __name__ == '__main__':
    unittest.main()
