#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name='Decopy',
    version='0.2.4.7',
    author='Maximiliano Curia',
    author_email='maxy@debian.org',
    description='Automatic debian/copyright Generator',
    long_description='''
    Decopy automates writing and updating the debian/copyright file.

    It reads all files in the source tree, analyzes the licenses and copyright
    messages included and generates the corresponding debian/copyright file.
    When the file already exists, decopy parses it to generate a more complete
    output.
    ''',
    url='https://salsa.debian.org/debian/decopy',
    license='ISC',
    requires=['debian', 'tqdm', 'xdg'],
    classifiers=[
        'Environment :: Console',
        'Programming Language :: Python :: 3',
        'Operating System :: POSIX :: Linux',
        'License :: OSI Approved :: ISC License (ISCL)',
    ],
    packages=find_packages(exclude=['tests', 'tests.*']),
    entry_points={
        'console_scripts': ['decopy = decopy.decopy:main'],
    },
    test_suite='tests',
)
